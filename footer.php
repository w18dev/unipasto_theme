		 <?php
	        $post = get_post('117'); 
	        $postId = $post->ID;
	        $logo_footer = get_field('logo_footer', $postId);
	        $copyright = get_field('copyright_footer', $postId);
	    ?>

		<div class="footer col-lg-12 col-xs-12 col-md-12 col-sm-12 ">
			<div class="row footer-container">			
				<div class="container">
					<div class="footer-copyright text-left col-lg-6 col-sm-8 col-xs-12 pull-left">
						<span><?php echo $copyright; ?></span>
					</div>
					<div class="footer-logo text-right col-lg-6 col-sm-4 col-xs-12 pull-right">
						<a><img src="<?php echo $logo_footer; ?>"></a>
					</div>
				</div>
			</div>
		</div>


		<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
		<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.11.3.min.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/champs.min.js"></script>
    </body>
</html>