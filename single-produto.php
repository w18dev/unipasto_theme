<?php include('header.php'); ?>
<?php
	while (have_posts()) {
	    the_post();
	    $postID = get_the_ID();
	    $fields = get_fields($postID);
	}
?>
	<div class="single-product col-lg-12 col-xs-12 col-md-12 col-sm-12">
		<div class="container">
			<div class="single-product-container">
				<div class="block-title">
					<h2>Produtos</h2>
				</div>
				<div class="product col-lg-12 col-sm-12 col-xs-12 col-md-12 ">
					 <div class="product-container">
					 	<div class="block-left col-lg-6 col-sm-6 col-xs-12 col-md-6">
					 		<div class="product-title">
					 			<h3><?php the_title(); ?></h3>
					 		</div>
					 		<div class="product-descript">
					 			<span><?php echo $fields['subtitulo']; ?></span>
					 		</div>
					 		<div class="product-esp">
					 			 <div class="product-esp-list">
					 			 	<ul>
					 			 		<?php foreach ($fields['lista_produtos'] as $kList => $vList) : ?>
						 			 		<li>
						 			 			<img src="<?php echo get_template_directory_uri(); ?>/images/icons/check.png">
						 			 			<span><?php echo $vList['lista_produtos_texto']; ?></span>
						 			 		</li>
						 			 	<?php endforeach; ?>
					 			 	</ul>
					 			 </div>
					 		</div>
					 	</div>
					 	<div class="block-right col-lg-6 col-sm-6 col-xs-12 col-md-6">
					 		<div class="block-right-container">
						 		<div class="product-image">
						 			<img src="<?php echo $fields['imagem']['url']; ?>">
						 		</div>
						 		<div class="product-block row">
							 		<div class="product-cat-down text-left pull-left">
							 			<?php if($fields['catalogo']!=""): ?>
							 				<a href="<?php echo $fields['catalogo']['url']; ?>">BAIXAR CATÁLOGO</a>
						 				<?php endif; ?>
							 		</div>
							 		<div class="product-aprovate-list pull-right">
							 			<ul>
							 				<?php if($fields['novidade']==1): ?>
							 					<li><img src="<?php echo get_template_directory_uri(); ?>/images/icons/icon-aprovate-1.png"></li>
							 				<?php endif; ?>
							 				<?php if($fields['embrapa']==1): ?>
							 					<li><img src="<?php echo get_template_directory_uri(); ?>/images/icons/icon-aprovate-2.png"></li>
							 				<?php endif; ?>
							 			</ul>
							 		</div>
						 		</div>
					 		</div>
					 	</div>
					 </div>
				</div>
				<div class="block-title">
					<h2>Mais informações</h2>
				</div>
				<div class="block-content">
					<?php echo $fields['mais_informacoes']; ?>
				</div>
			</div>
		</div>
		<div class="single-product-carousel col-lg-12 col-xs-12 col-md-12 col-sm-12">
			<div class="container">
				<div class="others-produtcts-title text-center">
					<h2>outros produtos</h2>
				</div>
				<div class="others-produtcts-carousel" id="others-products">
					<?php 
						wp_reset_query();
					    query_posts(array( 
					        'post_type' => 'produto'
					    ) );  
					?>
					<?php if ( have_posts() ) : while (have_posts()) : the_post(); ?>
						<?php $id = get_the_ID(); if($id!=$postID): ?>
						<?php $image = get_field_object( "field_5975e9df26c53", get_the_ID() ); ?>
						<div class="item">
							<div class="item-img">
								<a href="<?php the_permalink() ?>">
									<img src="<?php echo $image['value']['sizes']['medium']; ?>">
								</a>
							</div>
							<div class="item-text text-center">
								<a href="<?php the_permalink() ?>">
									<h3><?php the_title(); ?></h3>
								</a>
							</div>
						</div>
					<?php endif; endwhile; endif; ?>
				</div>
			</div>
		</div>
	</div>








<?php include('footer.php'); ?>