<?php
/**
 * @package Unipasto_theme_dev
 */
?>
<?php get_header() ?>

<?php
	$fields = get_fields(7);
?>
	<div class="main col-lg-12 col-xs-12 col-md-12 col-sm-12">
		<?php
			$post = get_post('113');
	        $banner_home = get_field('banner_home', $postId);
		?>
		<div class="cover cover-bkg" style="background-image: url(<?php echo $banner_home; ?>);">
		</div>
		<div class="about">
		 	<a name="sobre"></a>
		    <div class="container">
			 	<div class="about-container col-lg-12 col-xs-12 col-md-12 col-sm-12">
			 		<div class="row">
				 		<div class="about-container-logo text-center col-lg-3">
				 			<img src="<?php echo $fields['imagem']; ?>">
				 		</div>
				 		<div class="about-container-text text-left col-lg-9">
				 			<div class="about-resume">
					 			<p><?php echo $fields['resumo']; ?>
					 			</p>
				 			</div>
				 			<div class="about-full">
					 			<p><?php echo $fields['texto_completo']; ?>
					 			</p>
				 			</div>
					 		<div class="about-plus text-right">
					 			<a id="aboutLink">Leia mais</a>
					 		</div>
				 		</div>
			 		</div>
			 	</div>
			</div>
		</div>
		<div class="products">
			<a  name="produtos"></a>
			<div class="container">
				<div class="products-title text-center">
					<h2>Produtos</h2>
				</div>
				<?php 
				    query_posts(array( 
				        'post_type' => 'produto'
				    ) );  
				?>
				<div class="products-container col-lg-12 col-xs-12 col-md-12 col-sm-12">
					<div class="row">
						<?php if ( have_posts() ) : while (have_posts()) : the_post(); ?>
							<?php $image = get_field_object( "field_5975e9df26c53", get_the_ID() ); ?>
							<div class="product-block col-lg-4 col-xs-12">
								<div class="product-block-img">
									<a href="<?php the_permalink() ?>">
										<img src="<?php echo $image['value']['sizes']['medium']; ?>">
									</a>
								</div>
								<div class="product-block-name text-center">
									<a href="<?php the_permalink() ?>">
										<h3><?php the_title(); ?></h3>
									</a>
								</div>
							</div>
						<?php endwhile; endif; ?>
					</div>
				</div>
			</div>
		</div>
		<?php 
			wp_reset_query();
		    query_posts(array( 
		        'post_type' => 'associado'
		    ) );  
		    if ( have_posts() ) : while (have_posts()) : the_post();
		    	$idAssociado = get_the_ID();
		    	$associadosInfo[$idAssociado] = get_fields($idAssociado);
		    endwhile; endif;
		    foreach ($associadosInfo as $kAs => $vAs) :
				$associados[$vAs['tipo']][$kAs] = $vAs;
		    endforeach;
		?>
		<div class="associated" >
			<a name="associados"></a>
			<div class="container">
				<div class="associated-title text-center">
					<h2>Associados</h2>
				</div>
				<div class="associated-subtitle text-center">
					<p>Conheça as empresas associadas da Unipasto</p>
				</div>
				<div class="associated-container">
					<div class="associated-container-list ">
						<ul class="nav nav-tabs  text-center" role="tablist">
						   <li role="presentation" class="active"><a href="#ouro" aria-controls="ouro" role="tab" data-toggle="tab">Ouro</a></li>
						   <li role="presentation"><a href="#autorizados" aria-controls="autorizados" role="tab" data-toggle="tab">Autorizados</a></li>
						   <li role="presentation"><a href="#prata" aria-controls="prata" role="tab" data-toggle="tab">Prata</a></li>
						   <li role="presentation"><a href="#bronze" aria-controls="bronze" role="tab" data-toggle="tab">Bronze</a></li>
						</ul>
						<div class="tab-content">
						   <div role="tabpanel" class="tab-pane active" id="ouro">
						   		<div class="list-associated">
						   			<ul>
							   			<?php foreach ($associados['ouro'] as $kAO => $vAO) : ?>
							   				<li class="mantenedores-item col-lg-2 col-xs-6 col-sm-3 col-md-2">
							   					<a href="<?php echo $vAO['link']; ?>" class="mantenedores-item--link" target="_blank">
							   						<div class="mantenedores-item-container-img">
							   							<img src="<?php echo $vAO['imagem']['url']; ?>">
							   						</div>
							   						<div class="mantenedores-overlay">
							   						</div>
							   						<div class="mantenedores-info">
							   							<div class="mantenedores-info--img" style="background-image: url('<?php echo $vAO['imagem']['url']; ?>')">
							   							</div>
							   							<div class="mantenedores-info--content text-center">
							   								<?php echo $vAO['descricao']; ?>
							   							</div>
							   						</div>
							   					</a>
							   				</li>
								   		<?php endforeach; ?>
						   			</ul>
						   		</div>
						   </div>
						   <div role="tabpanel" class="tab-pane" id="autorizados">
						   		<div class="list-associated">
						   			<ul>
							   			<?php foreach ($associados['autorizados'] as $kAO => $vAO) : ?>
							   				<li class="mantenedores-item col-lg-2 col-xs-6 col-sm-3 col-md-2">
							   					<a href="<?php echo $vAO['link']; ?>" class="mantenedores-item--link" target="_blank">
							   						<div class="mantenedores-item-container-img">
							   							<img src="<?php echo $vAO['imagem']['url']; ?>">
							   						</div>
							   						<div class="mantenedores-overlay">
							   						</div>
							   						<div class="mantenedores-info">
							   							<div class="mantenedores-info--img" style="background-image: url('<?php echo $vAO['imagem']['url']; ?>')">
							   							</div>
							   							<div class="mantenedores-info--content text-center">
							   								<?php echo $vAO['descricao']; ?>
							   							</div>
							   						</div>
							   					</a>
							   				</li>
								   		<?php endforeach; ?>
						   			</ul>
						   		</div>
						   </div>
						   <div role="tabpanel" class="tab-pane" id="prata">
						   		<div class="list-associated">
						   			<ul>
							   			<?php foreach ($associados['prata'] as $kAO => $vAO) : ?>
							   				<li class="mantenedores-item col-lg-2 col-xs-6 col-sm-3 col-md-2">
							   					<a href="<?php echo $vAO['link']; ?>" class="mantenedores-item--link" target="_blank">
							   						<div class="mantenedores-item-container-img">
							   							<img src="<?php echo $vAO['imagem']['url']; ?>">
							   						</div>
							   						<div class="mantenedores-overlay">
							   						</div>
							   						<div class="mantenedores-info">
							   							<div class="mantenedores-info--img" style="background-image: url('<?php echo $vAO['imagem']['url']; ?>')">
							   							</div>
							   							<div class="mantenedores-info--content text-center">
							   								<?php echo $vAO['descricao']; ?>
							   							</div>
							   						</div>
							   					</a>
							   				</li>
								   		<?php endforeach; ?>
						   			</ul>
						   		</div>
						   </div>
						   <div role="tabpanel" class="tab-pane" id="bronze">
						   		<div class="list-associated">
						   			<ul>
						   				<?php foreach ($associados['prata'] as $kAO => $vAO) : ?>
							   				<li class="mantenedores-item col-lg-2 col-xs-6 col-sm-3 col-md-2">
							   					<a href="<?php echo $vAO['link']; ?>" class="mantenedores-item--link" target="_blank">
							   						<div class="mantenedores-item-container-img">
							   							<img src="<?php echo $vAO['imagem']['url']; ?>">
							   						</div>
							   						<div class="mantenedores-overlay">
							   						</div>
							   						<div class="mantenedores-info">
							   							<div class="mantenedores-info--img" style="background-image: url('<?php echo $vAO['imagem']['url']; ?>')">
							   							</div>
							   							<div class="mantenedores-info--content text-center">
							   								<?php echo $vAO['descricao']; ?>
							   							</div>
							   						</div>
							   					</a>
							   				</li>
								   		<?php endforeach; ?>
						   			</ul>
						   		</div>
						   </div>
					    </div> 
					</div>
				</div>
			</div>
		</div>
		<?php
			$resultado = file_get_contents('https://www.youtube.com/feeds/videos.xml?channel_id=UC8mDF5mWNGE-Kpfcvnn0bUg');
			$xml = new SimpleXMLElement($resultado);
			$videos = array();
			$count = 0;

			foreach ($xml->entry AS $video) {
				$url = (string)$video->link['href'];
				parse_str(parse_url($url, PHP_URL_QUERY), $params);
				$id = $params['v'];

				if($count<3):
					$videos[] = array(
						'id' => $id,
						'titulo' => (string)$video->title,
						'thumbnail' => 'http://i' . rand(1, 4) .'.ytimg.com/vi/'. $id .'/hqdefault.jpg',
						'embed' => 'https://www.youtube.com/embed/'.$id,
						'url' => $url
					);
				endif;

				$count++;
			}
		?>
		<div class="movie movie-cover">
			<a name="videos"></a>
			<div class="container">
				<div class="movie-container">				
					<div class="movie-container-title text-center">
						<h2>Vídeos</h2>
					</div>
					<div class="movie-carousel">
						<div id="movie-carousel">
							<?php foreach ($videos as $kVid => $vVid) : ?>
								<div class="item">
									<div class="item-movie">
										<iframe width="100%" height="496" src="<?php echo $vVid['embed']; ?>" frameborder="0" allowfullscreen></iframe>
									</div>
								</div>
							<?php endforeach; ?> 						
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
	        $post = get_post('40'); 
	        $postId = $post->ID;
	        $fone_fax = get_field('fone_fax', $postId);
	        $endereco = get_field('endereco_contato', $postId);
	        $cep = get_field('cep_contato', $postId);
	        $email = get_field('email_contato', $postId);
	        $link_download = get_field('link_baixe_o_aplicativo', $postId);
	    ?>
		<div class="related">
			<a name="contato"></a>
			<div class="row">
				<div class="related-container col-lg-12 col-xs-12 col-sm-12 col-md-12">
					<div class="contact col-lg-6 col-xs-12">
						<div class="contact-container">
							<form>
								<legend>FALE CONOSCO</legend>
								<div class="form-group">
								    <label class="sr-only" for="nome">Nome</label>
								    <input type="text" class="form-control" id="nome" placeholder="NOME" required="">
								</div>
								<div class="form-group">
								    <label class="sr-only" for="email">E-mail</label>
								    <input type="email" class="form-control" id="email" placeholder="SEU E-MAIL" required="">
								</div>
								<div class="form-group">
								    <label class="sr-only" for="mensagem">Mensagem</label>
								    <textarea class="form-control text" rows="3" placeholder="MENSAGEM" required></textarea>
								</div>
								<div class="form-group">
								    <button class="btn btn-success" type="submit">ENVIAR</button>
								</div>
							</form>
						</div>
					</div>
					<div class="locate col-lg-6 col-xs-12">
						<div class="locate-container">
							<div class="block-phone">
								<span>Fone e Fax:</span>
								<span><?php echo $fone_fax;?></span>
							</div>
							<div class="block-locate">
								<div class="block-locate-info">
									<p><?php echo $endereco;?></p>
								</div>
								<div class="block-locate-info">
									<p>CEP:<?php echo $cep;?></p>
								</div>
								<div class="block-locate-info">
									<p>E-mail: <?php echo $email;?></p>
								</div>
							</div>
							<div class="block-content">
								<a href="<?php echo $link_download;?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/logo-pasto-certo.png">
								BAIXE O APLICATIVO</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>




<?php include('footer.php'); ?>