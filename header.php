<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/champs.min.css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">
        <!--[if lt IE 8]>
            <script type="text/javascript" src="js/html5shiv.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/modernizr-2.6.2.min.js"></script>
    </head>
<body>
    <?php
        $post = get_post('113'); 
        $postId = $post->ID;
        $logo_header = get_field('logo_header', $postId);
        $link_area_rest = get_field('area_restrita_header', $postId);
    ?>
    <header class="header col-xs-12 col-lg-12 col-md-12 col-sm-12">       
        <div class="row header-container">
            <div class="block-logo text-right col-lg-4 col-xs-6 col-md-4 pull-left">
                <h1><a href="<?php echo get_home_url(); ?>"><img src="<?php echo $logo_header; ?>"></a></h1>
            </div>
            <div class="block-list text-left col-lg-8 col-xs-6 col-md-8 pull-left">
                <div class="icon-menu hidden-lg hidden-md text-right">
                    <i class="fa fa-bars" id="open-menu"></i>
                </div>
                <div class="menu menu-responsive">
                    <div class="close-menu hidden-lg hidden-md text-right">
                        <i class="fa fa-close" id="close-menu"></i>
                    </div>
                    <ul class="menu-links">
                        <li><a href="#sobre">SOBRE NÓS</a></li>    
                        <li><a href="#produtos">PRODUTOS</a></li>    
                        <li><a href="#associados">ASSOCIADOS</a></li>    
                        <li><a href="#videos">VÍDEOS</a></li>    
                        <li><a href="#contato">CONTATO</a></li>    
                        <li><a href="<?php echo $link_area_rest; ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/icon-key.png"><span>ÁREA RESTRITA</span></a></li>     
                    </ul>
                </div>
            </div>
        </div>
    </header>
