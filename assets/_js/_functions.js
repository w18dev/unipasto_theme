    function MovieCarousel(){
        if(document.getElementById("movie-carousel") != null){
            var carouselGaleria = $("#movie-carousel");
            carouselGaleria.owlCarousel({
                items:1,
                nav: true,
                navText: ['<i class="icon icon-left"></i>', '<i class="icon icon-right"></i>'],
            });
        }
    }   

    function OthersProducts(){
        if(document.getElementById("others-products") != null){
            var carouselGaleria = $("#others-products");
            carouselGaleria.owlCarousel({
                items:3,
                nav: true,
                navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
                responsive:{
                    320: {
                        items: 1,
                    },
                    480: {
                        items: 2,
                    },
                    800: {
                        items: 3,
                    },
                }
            });
        }
    }

    function openMenuResponsive(){
        $('#open-menu').on('click', function(){
            if($('.menu-responsive').hasClass('open')){
                $('.menu-responsive').removeClass('open');
            }
            else{
                $('.menu-responsive').addClass('open');
            }
        });

        $('#close-menu').on('click', function(){
            $('.menu-responsive').removeClass('open');
        });

        $(window).scroll(function(){
            if($('.menu-responsive').hasClass('open')){
                $('.menu-responsive').removeClass('open');
            }
        });
    }