$(window).load(function(){
    MovieCarousel();
    OthersProducts();
    openMenuResponsive();

    $('#aboutLink').on('click', function(){
    	$('.about-resume').css({'display':'none'});
    	$('.about-full').css({'display':'block'});
    	$('#aboutLink').css({'display':'none'});
    });

    $(".menu-links li").on("click", function() {
    	$(".menu-links li").removeClass("active");
    	$(this).addClass("active");
    });
    /*
		#######################
			MENU FIXO SCROOL
		#######################
	*/
		$(window).scroll(function(){
			var top = ($(window).scrollTop());
			if(top >= 120){
				$('.header').addClass('header-scroll');
			}
			else
			{
				$('.header').removeClass('header-scroll');
			}
		});

	/*
		#######################
			MENU FIXO SCROOL
		#######################
	*/
});

function scrollLink(){
    if(window.location.hash==="#sobre"){
    	$('html,body').animate({scrollTop:$("#sobre").offset().top-200}, 1500); 
    }
    else{
    	if(window.location.hash==="#produtos"){
    		$('html,body').animate({scrollTop:$("#produtos").offset().top-250}, 1500); 
    	}
    	else{
    		if(window.location.hash==="#associados"){
    			$('html,body').animate({scrollTop:$("#associados").offset().top-200}, 1500); 
    		}
    		else{
    			if(window.location.hash==="#videos"){
    				$('html,body').animate({scrollTop:$("#videos").offset().top-120}, 1500); 
    			}
    			else{
    				if(window.location.hash==="#contato"){
    					$('html,body').animate({scrollTop:$("#contato").offset().top-120}, 1500); 
    				}
    			}
    		}
    	}
    }
}